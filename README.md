# OSC_BetterHeaders
This is an OS Commerce addon which is a quick and dirty method for replacing the default "Let's See What We Have Here" heading title on the category listing and the manufacturer listing.

For more information see http://addons.oscommerce.com/info/6981

## Instructions

1. Download the latest version ()
1. Unzip the package
1. Switch to the "OSC_BetterHeaders" directory
1. Follow the directions in "better_headers_1.1.txt

