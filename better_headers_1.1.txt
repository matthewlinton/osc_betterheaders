-==================================================================
Better Heading Titles 1.1
==================================================================
Matthew Linton
New York, US
2009-09-17

Introduction:

This contribution is a quick and dirty method for replacing the default "Let's See What We Have Here" heading title on the category listing and the manufacturer listing.  It is not intended to replace the heading title on the deafult index page.

The Following resources have been modified:

1) catalog/index.php

=============================================================================================================

STEP 1:
	Make backups of the above mentioned files

*************************************************************************************************************

STEP 2: 
	in "catalog/index.php"

	### FIND THE FIRST TWO INSTANCES OF ####

	<td class="pageHeading"><?php echo HEADING_TITLE;?></td>

	### REPLACE WITH ##################

	<!-- BETTER HEADING TITLES BOF  -->
	<?php
	// Use manufacturers name or category title insted of generic text
   	if (isset($HTTP_GET_VARS['manufacturers_id'])) {
   	   $product_manufacturer_query = tep_db_query("select manufacturers_id, manufacturers_name from manufacturers where manufacturers_id = " . $HTTP_GET_VARS['manufacturers_id']);
	      $product_manufacturer = tep_db_fetch_array($product_manufacturer_query);
	      $custom_title = $product_manufacturer['manufacturers_name'];
	   } elseif (isset($cPath)) {
	      $res = explode('_', $cPath);
              $count = count($res) - 1;
              if ( ! empty($res[$count]) ) { $cPath = $res[$count]; }
	      $product_category_query = tep_db_query("select categories_id, categories_name from categories_description where categories_id = " . $cPath );
	      $product_category = tep_db_fetch_array($product_category_query);
	      $custom_title = $product_category['categories_name'];
	   } else {
	      $custom_title = HEADING_TITLE;
	   }
	?>
            <td class="pageHeading"><?php echo $custom_title; ?></td>
	<!-- BETTER HEADING TITLES EOF -->


=============================================================================================================

Change log:

v1.1	Fixed issue with sub categories
v1.0	Initial Release

